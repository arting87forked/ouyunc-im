package com.ouyunc.im.protocol;


import com.alibaba.fastjson2.JSON;
import com.ouyunc.im.base.MissingPacket;
import com.ouyunc.im.base.SendCallback;
import com.ouyunc.im.base.SendResult;
import com.ouyunc.im.codec.MqttWebSocketCodec;
import com.ouyunc.im.constant.CacheConstant;
import com.ouyunc.im.constant.IMConstant;
import com.ouyunc.im.constant.enums.MessageTypeEnum;
import com.ouyunc.im.constant.enums.SendStatus;
import com.ouyunc.im.context.IMServerContext;
import com.ouyunc.im.exception.handler.GlobalExceptionHandler;
import com.ouyunc.im.handler.*;
import com.ouyunc.im.helper.MqttHelper;
import com.ouyunc.im.innerclient.pool.IMInnerClientPool;
import com.ouyunc.im.packet.Packet;
import com.ouyunc.im.packet.message.ExtraMessage;
import com.ouyunc.im.packet.message.InnerExtraData;
import com.ouyunc.im.packet.message.Message;
import com.ouyunc.im.utils.MapUtil;
import com.ouyunc.im.utils.ReaderWriterUtil;
import com.ouyunc.im.utils.SocketAddressUtil;
import com.ouyunc.im.utils.SystemClock;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.pool.ChannelPool;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @Author fangzhenxun
 * @Description: 协议类型
 **/
public enum Protocol {

    // 处理ws/wss,这里相当于关键入口
    WS((byte) 1, (byte) 1, "websocket 协议，版本号为1") {
        @Override
        public void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap) {
            ctx.pipeline()
                    //10 * 1024 * 1024
                    .addLast(IMConstant.WS_FRAME_AGGREGATOR, new WebSocketFrameAggregator(Integer.MAX_VALUE))
                    //10485760
                    .addLast(IMConstant.WS_SERVER_PROTOCOL_HANDLER, new WebSocketServerProtocolHandler(IMServerContext.SERVER_CONFIG.getWebsocketPath(), null, true, Integer.MAX_VALUE))
                    // 转换成包packet,内部消息传递都是以packet 进行处理
                    .addLast(IMConstant.CONVERT_2_PACKET, new Convert2PacketHandler())
                    // 添加监控处理逻辑
                    .addLast(IMConstant.MONITOR_HANDLER, new MonitorHandler())
                    // 在业务处理之前可以进行登录认证处理，登录认证处理，如果不需要登录处理，可在配置文件中配置，不需要在这里处理
                    // qos 前置处理
                    .addLast(IMConstant.QOS_HANDLER_PRE, new QosHandler())
                    // 前置处理
                    .addLast(IMConstant.PRE_HANDLER, new PacketPreHandler())
                    // 业务处理
                    .addLast(IMConstant.WS_HANDLER, new WsServerHandler())
                    // 后置处理
                    .addLast(IMConstant.POST_HANDLER, new PacketPostHandler())
                    // qos 后置处理
                    .addLast(IMConstant.QOS_HANDLER_POST, new QosHandler())
                    // 托管处理器
                    .addLast(IMConstant.TRUSTEESHIP_HANDLER, new TrusteeshipHandler());
            // 判断是否需要开启客户端心跳如果需要则开启客户端心跳，由于心跳消息不需要登录就可以，所以放在登录认证处理器前面
            // 在最后添加异常处理器
            ctx.pipeline().addLast(IMConstant.GLOBAL_EXCEPTION, new GlobalExceptionHandler());
            // 移除协议分发器
            ctx.pipeline().remove(IMConstant.HTTP_DISPATCHER_HANDLER);
            // 调用当前handler的下一个handle的active，注意与ctx.pipeline().fireChannelActive()
            ctx.fireChannelActive();
        }

        /**
         * @Author fangzhenxun
         * @Description ws/wss 最终发送的消息方法
         * @param packet
         * @param to  接受者唯一标识，这里了也可以从packet获取但是还需要，进行消息的判断与解析（主要不同是，序列化不同的消息类型）
         * @return void
         */
        @Override
        public void doSendMessage(Packet packet, String to, SendCallback sendCallback) {
            // 清理集群消息投递过程中产生的路由记录
            Message message = (Message) packet.getMessage();
            // 这里应该不会为null
            ExtraMessage extraMessage = JSON.parseObject(message.getExtra(), ExtraMessage.class);
            InnerExtraData innerExtraData = extraMessage.getInnerExtraData();
            String appKey = innerExtraData.getAppKey();
            message.setExtra(extraMessage.getOutExtraData());
            try {
                // 这里可以处理具体发送多设备的在线消息，离线以及在线
                // 需要考虑多设备登录？离线消息如何嵌入？，多种消息类型进行统一处理
                final ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer();
                ReaderWriterUtil.writePacketInByteBuf(packet, byteBuf);
                //从用户注册表中，获取用户对应的channel然后将消息写出去
                ChannelFuture sendResultChannelFuture = IMServerContext.USER_REGISTER_TABLE.get(to).writeAndFlush(new BinaryWebSocketFrame(byteBuf));
                sendResultChannelFuture.addListener((ChannelFutureListener) channelFuture -> {
                    if (channelFuture.isDone()) {
                        if (channelFuture.isSuccess()) {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_OK).packet(packet).build());
                        }else {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(channelFuture.cause()).build());
                        }
                    }
                });
            } catch (Exception e) {
                log.error("消息packet: {} 发送给用户: {} 失败!", packet, to);
                // 消息丢失
                if (packet.getMessageType() == MessageTypeEnum.IM_PRIVATE_CHAT.getValue() || packet.getMessageType() == MessageTypeEnum.IM_GROUP_CHAT.getValue()) {
                    // 对于多端的情况
                    long now = SystemClock.now();
                    IMServerContext.MISSING_MESSAGES_CACHE.addZset(CacheConstant.OUYUNC + CacheConstant.APP_KEY + appKey + CacheConstant.COLON + CacheConstant.IM_MESSAGE + CacheConstant.FAIL + CacheConstant.FROM + message.getFrom() + CacheConstant.COLON + CacheConstant.TO + to, new MissingPacket(packet, IMServerContext.SERVER_CONFIG.getLocalServerAddress(), now), now);
                }
                sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(e).build());
            }
        }
    },


    //处理 http/https
    HTTP((byte) 3, (byte) 1, "http协议，版本号为1") {
        @Override
        public void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap) {

        }

        @Override
        public void doSendMessage(Packet packet, String to, SendCallback sendCallback) {
            // do something
        }
    },

    // 目前该协议不对外开放只作为集群内部协议使用，可以对接jt 818,或者其他物联网的通信，字节扩充，
    OUYUNC((byte) 5, (byte) 1, "自定义ouyunc协议，版本号为1") {
        @Override
        public void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap) {
            ctx.pipeline()
                    // 上一个packet编解码处理器，处理后，会在这里交给包转换器来转换
                    // 转换成包packet，这里为了做兼容客户端心跳
                    .addLast(IMConstant.CONVERT_2_PACKET, new Convert2PacketHandler())
                    // 添加一个集群中处理消息路由的处理器，这样就不需要在业务处理器中都写一下了
                    .addLast(IMConstant.PACKET_CLUSTER_ROUTER, new PacketClusterRouterHandler())
                    // 集群内部/外部业务处理
                    .addLast(IMConstant.OUYUNC_IM_HANDLER, new OuyuncServerHandler())
                    // 在最后添加异常处理器
                    .addLast(IMConstant.GLOBAL_EXCEPTION, new GlobalExceptionHandler())
                    // 移除协议分发器
                    .remove(IMConstant.PACKET_DISPATCHER_HANDLER);
            // 调用下一个handle的active
            ctx.fireChannelActive();
        }

        /**
         * 直接获取对应的channel然后写出去,主要用于集群中心跳
         * @param packet
         * @param to
         */
        @Override
        public void doSendMessage(Packet packet, String to, SendCallback sendCallback) {
            ChannelPool channelPool = MapUtil.mergerMaps(IMServerContext.CLUSTER_ACTIVE_SERVER_REGISTRY_TABLE.asMap(), IMServerContext.CLUSTER_GLOBAL_SERVER_REGISTRY_TABLE.asMap()).get(to);
            if (channelPool == null) {
                log.warn("有新的服务 {} 加入集群，正在尝试与其确认ack", to);
                channelPool = IMInnerClientPool.singleClientChannelPoolMap.get(SocketAddressUtil.convert2SocketAddress(to));
            }
            ChannelPool finalChannelPool = channelPool;
            Future<Channel> channelFuture = finalChannelPool.acquire();
            channelFuture.addListener(new FutureListener<Channel>() {
                @Override
                public void operationComplete(Future<Channel> future) throws Exception {
                    if (future.isDone()) {
                        // 判断是否连接成功
                        if (future.isSuccess()) {
                            Channel channel = future.getNow();
                            // 给该通道打上标签(如果该通道channel 上有标签则不需要再打标签),打上标签的目的，是为了以后动态回收该channel,保证核心channel数
                            AttributeKey<Integer> channelTagPoolKey = AttributeKey.valueOf(IMConstant.CHANNEL_TAG_POOL);
                            final Integer channelPoolHashCode = channel.attr(channelTagPoolKey).get();
                            if (channelPoolHashCode == null) {
                                channel.attr(channelTagPoolKey).set(finalChannelPool.hashCode());
                            }
                            // 客户端将数据写出到中介管道中
                            ChannelFuture sendResultChannelFuture = channel.writeAndFlush(packet);
                            sendResultChannelFuture.addListener((ChannelFutureListener) channelFuture -> {
                                if (channelFuture.isDone()) {
                                    if (channelFuture.isSuccess()) {
                                        sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_OK).packet(packet).build());
                                    }else {
                                        sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(channelFuture.cause()).build());
                                    }
                                }
                            });
                            // 用完后进行释放掉
                            finalChannelPool.release(channel);
                        } else {
                            // 获取失败
                            Throwable e = future.cause();
                            log.error("获取远端地址失败：{}", e.getMessage());
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(e).build());
                        }

                    }
                }
            });
        }
    },


    //mqtt
    MQTT((byte) 6, (byte) 4, "mqtt协议，版本号为3.1.1") {
        @Override
        public void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap) {
            ctx.pipeline()
                    .addLast(IMConstant.MQTT_DECODER, new MqttDecoder())
                    .addLast(IMConstant.MQTT_ENCODER, MqttEncoder.INSTANCE)
                    .addLast(IMConstant.CONVERT_2_PACKET, new Convert2PacketHandler())
                    // 前置处理
                    .addLast(IMConstant.PRE_HANDLER, new PacketPreHandler())
                    // 业务处理
                    .addLast(IMConstant.MQTT_SERVER, new MqttBrokerServerHandler())
                    // 后置处理
                    .addLast(IMConstant.POST_HANDLER, new PacketPostHandler())
                    .remove(IMConstant.HTTP_DISPATCHER_HANDLER);
            ctx.pipeline().addLast(IMConstant.GLOBAL_EXCEPTION, new GlobalExceptionHandler());
            // 调用下一个handle的active
            ctx.fireChannelActive();
        }

        @Override
        public void doSendMessage(Packet packet, String to, SendCallback sendCallback) {
            if (log.isDebugEnabled()) {
                log.debug("mqtt 正在发送消息...");
            }
            Message message = (Message) packet.getMessage();
            // 这里应该不会为null
            ExtraMessage extraMessage = JSON.parseObject(message.getExtra(), ExtraMessage.class);
            InnerExtraData innerExtraData = extraMessage.getInnerExtraData();
            String appKey = innerExtraData.getAppKey();
            message.setExtra(extraMessage.getOutExtraData());
            try {
                //从用户注册表中，获取用户对应的channel然后将消息写出去
                ChannelFuture sendResultChannelFuture = IMServerContext.USER_REGISTER_TABLE.get(to).writeAndFlush(MqttHelper.unwrapPacket2Mqtt(packet));
                sendResultChannelFuture.addListener((ChannelFutureListener) channelFuture -> {
                    if (channelFuture.isDone()) {
                        if (channelFuture.isSuccess()) {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_OK).packet(packet).build());
                        }else {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(channelFuture.cause()).build());
                        }
                    }
                });
            } catch (Exception e) {
                log.error("消息packet: {} 发送给客户端: {} 失败!,原因：{}", packet, to, e.getMessage());
                // 消息丢失 @todo 后面处理
                sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(e).build());
            }
        }
    },


    //mqtt_ws 协议
    MQTT_WS((byte) 7, (byte) 4, "基于websocket的mqtt协议，版本号为3.1.1") {
        @Override
        public void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap) {
            ctx.pipeline()
                    //10 * 1024 * 1024
                    .addLast(IMConstant.WS_FRAME_AGGREGATOR, new WebSocketFrameAggregator(Integer.MAX_VALUE))
                    //10485760
                    .addLast(IMConstant.WS_SERVER_PROTOCOL_HANDLER, new WebSocketServerProtocolHandler(IMServerContext.SERVER_CONFIG.getWebsocketPath(), IMConstant.WEBSOCKET_SUB_PROTOCOLS, true, Integer.MAX_VALUE))
                    .addLast(IMConstant.MQTT_WEBSOCKET_CODEC, new MqttWebSocketCodec())
                    .addLast(IMConstant.MQTT_DECODER, new MqttDecoder())
                    .addLast(IMConstant.MQTT_ENCODER, MqttEncoder.INSTANCE)
                    .addLast(IMConstant.CONVERT_2_PACKET, new Convert2PacketHandler())
                    // 前置处理
                    .addLast(IMConstant.PRE_HANDLER, new PacketPreHandler())
                    // 业务处理
                    .addLast(IMConstant.MQTT_SERVER, new MqttBrokerServerHandler())
                    // 后置处理
                    .addLast(IMConstant.POST_HANDLER, new PacketPostHandler())
                    .remove(IMConstant.HTTP_DISPATCHER_HANDLER);

            ctx.pipeline().addLast(IMConstant.GLOBAL_EXCEPTION, new GlobalExceptionHandler());
            // 调用下一个handle的active
            ctx.fireChannelActive();
        }

        @Override
        public void doSendMessage(Packet packet, String to, SendCallback sendCallback) {
            if (log.isDebugEnabled()) {
                log.debug("mqtt-ws 正在发送消息...");
            }
            Message message = (Message) packet.getMessage();
            // 这里应该不会为null
            ExtraMessage extraMessage = JSON.parseObject(message.getExtra(), ExtraMessage.class);
            InnerExtraData innerExtraData = extraMessage.getInnerExtraData();
            String appKey = innerExtraData.getAppKey();
            message.setExtra(extraMessage.getOutExtraData());
            try {
                //从用户注册表中，获取用户对应的channel然后将消息写出去
                ChannelFuture sendResultChannelFuture = IMServerContext.USER_REGISTER_TABLE.get(to).writeAndFlush(MqttHelper.unwrapPacket2Mqtt(packet));
                sendResultChannelFuture.addListener((ChannelFutureListener) channelFuture -> {
                    if (channelFuture.isDone()) {
                        if (channelFuture.isSuccess()) {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_OK).packet(packet).build());
                        }else {
                            sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(channelFuture.cause()).build());
                        }
                    }
                });
            } catch (Exception e) {
                log.error("消息packet: {} 发送给客户端: {} 失败!,原因：{}", packet, to, e.getMessage());
                // 消息丢失 @todo 后面处理
                sendCallback.onCallback(SendResult.builder().sendStatus(SendStatus.SEND_FAIL).packet(packet).exception(e).build());
            }
        }
    };


    private static Logger log = LoggerFactory.getLogger(Protocol.class);

    /**
     * 协议编号
     */
    private byte protocol;

    /**
     * 协议版本
     */
    private byte version;

    /**
     * 协议描述
     */
    private String description;

    Protocol(byte protocol, byte version, String description) {
        this.protocol = protocol;
        this.version = version;
        this.description = description;
    }

    public static Protocol prototype(short protocol, byte protocolVersion) {
        for (Protocol protocolEnum : Protocol.values()) {
            if (protocolEnum.protocol == protocol && protocolEnum.version == protocolVersion) {
                return protocolEnum;
            }
        }
        return null;
    }

    public byte getProtocol() {
        return protocol;
    }

    public void setProtocol(byte protocol) {
        this.protocol = protocol;
    }

    public byte getVersion() {
        return version;
    }

    public void setVersion(byte version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * @Author fangzhenxun
     * @Description 协议分发器
     * @param ctx
     * @param queryParamsMap 请求参数
     * @return void
     */
    public abstract void doDispatcher(ChannelHandlerContext ctx, Map<String, Object> queryParamsMap);

    /**
     * @Author fangzhenxun
     * @Description
     * @param packet 消息包
     * @param to 接受者
     * @param sendCallback 这个发送的回调，针对成功来说，只是理论上的成功，因为writeAndFlush 本身就是异步的，加上网络的不稳定性，很难严格意义上的判断发送成功
     * @return void
     */
    public abstract void doSendMessage(Packet packet, String to, SendCallback sendCallback);
}
