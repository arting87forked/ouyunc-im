package com.ouyunc.im.processor;

import com.ouyunc.im.constant.IMConstant;
import com.ouyunc.im.constant.enums.DeviceEnum;
import com.ouyunc.im.constant.enums.MessageContentEnum;
import com.ouyunc.im.constant.enums.MessageTypeEnum;
import com.ouyunc.im.context.IMServerContext;
import com.ouyunc.im.helper.MessageHelper;
import com.ouyunc.im.helper.MqttHelper;
import com.ouyunc.im.packet.Packet;
import com.ouyunc.im.packet.message.Message;
import com.ouyunc.im.packet.message.Target;
import com.ouyunc.im.utils.SnowflakeUtil;
import com.ouyunc.im.utils.SystemClock;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.mqtt.MqttFixedHeader;
import io.netty.handler.codec.mqtt.MqttMessageFactory;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author fangzhenxun
 * @Description: 外部客户端心跳消息
 **/
public class PingPongMessageProcessor extends AbstractMessageProcessor {
    private static Logger log = LoggerFactory.getLogger(PingPongMessageProcessor.class);

    @Override
    public MessageTypeEnum messageType() {
        return MessageTypeEnum.IM_PING_PONG;
    }


    /**
     * 处理外部客户端 ping-pong 消息
     *
     * @param ctx
     * @param packet
     */
    @Override
    public void doProcess(ChannelHandlerContext ctx, Packet packet) {
        if (log.isDebugEnabled()) {
            log.debug("PingPongMessageProcessor 正在处理心跳 {} ...", packet);
        }
        // 可能在三次之内再次发起心跳，此时需要清除 之前心跳超时次数的历史记录
        AttributeKey<Integer> channelTagReadTimeoutKey = AttributeKey.valueOf(IMConstant.CHANNEL_TAG_READ_TIMEOUT);
        ctx.channel().attr(channelTagReadTimeoutKey).set(null);
        // 发送pong
        // 处理心跳消息
        Message heartBeatMessage = (Message) packet.getMessage();
        String from = heartBeatMessage.getFrom();
        heartBeatMessage.setFrom(IMServerContext.SERVER_CONFIG.getLocalServerAddress());
        heartBeatMessage.setTo(from);
        // 兼容mqtt协议的心跳
        if (MessageContentEnum.MQTT.type() == heartBeatMessage.getContentType()) {
            heartBeatMessage.setContent(MqttHelper.gson().toJson(MqttMessageFactory.newMessage(new MqttFixedHeader(MqttMessageType.PINGRESP, false, MqttQoS.AT_MOST_ONCE, false, 0), null, null)));
        }
        heartBeatMessage.setContentType(MessageContentEnum.PONG_CONTENT.type());
        heartBeatMessage.setCreateTime(SystemClock.now());
        packet.setPacketId(SnowflakeUtil.nextId());
        packet.setIp(IMServerContext.SERVER_CONFIG.getIp());
        // 写回的是websocket还是其他类型的数据
        MessageHelper.asyncSendMessage(packet, Target.newBuilder().targetIdentity(from).deviceEnum(DeviceEnum.getDeviceEnumByValue(packet.getDeviceType())).build());
    }
}
