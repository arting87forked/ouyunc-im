package com.ouyunc.im.constant;

/**
 * @Author fangzhenxun
 * @Description: 缓存相关常量类
 **/
public class CacheConstant {
    // 冒号
    public static final String COLON  = ":";
    // 横杠
    public static final String CROSSBAR  = "-";
    // 锁前缀
    public static final String LOCK = "lock:";
    // ouyunc 公共前缀
    public static final String OUYUNC = "ouyunc:";
    //  im公共前缀
    public static final String IM = "im:";
    // 平台的 唯一标识key 公共前缀
    public static final String APP_KEY = "app-key:";
    // 集群服务下线hash key
    public static final String CLUSTER_SERVER = "cluster:server:";
    // 群组
    public static final String GROUP = "group:";
    // 用户
    public static final String USER = "user:";
    // 发送失败消息
    public static final String FAIL = "fail:";
    // 离线消息
    public static final String OFFLINE = "offline:";
    // 消息发送者
    public static final String FROM = "from:";
    // 消息接收者
    public static final String TO = "to:";
    // 联系人
    public static final String CONTACT = "contact:";
    // 黑名单
    public static final String BLACK_LIST = "black-list:";
    // 已读回执
    public static final String READ_RECEIPT = "read-receipt:";
    // 登录
    public static final String LOGIN = "login:";
    // 发送
    public static final String SEND = "send:";
    // 接收
    public static final String RECEIVE = "receive:";
    // 信箱
    public static final String TIME_LINE = "time_line:";
    // 消息相关公共前缀
    public static final String IM_MESSAGE = "im:message:";
    // 用户相关公共前缀
    public static final String IM_USER = "im:user:";
    // 用户联系人（好友）相关前缀
    public static final String FRIEND = "friend:";
    // 群成员相关前缀
    public static final String MEMBERS = ":members";
    // 分布式锁的同意加入群或拒绝加入群
    public static final String REFUSE_AGREE = "refuse-agree:";
    // im app detail 信息数据
    public static final String APP = "app:";
    // im app 连接
    public static final String CONNECTIONS = "connections";
    // 好友请求
    public static final String FRIEND_REQUEST = "friend-request:";
    // 群组请求
    public static final String GROUP_REQUEST = "group-request:";
    // mqtt
    public static final String MQTT = "mqtt:";
    // topic
    public static final String TOPIC = "topic";
    // 聊天会话
    public static final String SESSION = "session:";
}
