package com.ouyunc.im.context;

import com.ouyunc.im.config.IMConfig;

/**
 * @Author fangzhenxun
 * @Description: IM 上线文
 **/
public class IMContext {

    /**
     * IM 公共服务配置文件
     */
    public static IMConfig IM_CONFIG;
}
